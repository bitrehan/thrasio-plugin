<?php
function thrasio_register_deals_type() {

	$labels = array(
		'name' => __( 'Thrasio', THRASIODOMAIN ),
		'singular_name' => __( 'Deals Page', THRASIODOMAIN ),
    'add_new' => 'Add Deals Page',
    'add_new_item' => 'Add Products to Deals Page',
    'edit_item' => 'Edit Deals Page',
    'view_item' => 'View Deals Page'
	);

	$args = array(
		'labels' => $labels,
		'public' => true,
    'show_in_menu' => true,
    'menu_icon' => 'http://localhost:8888/test-site/wp-content/plugins/thrasio/inc/menu_icon.png',
    'supports' => array( 'title', 'editor', 'thumbnail' )
	);

	register_post_type( 'deals', $args );
}
