<?php
/**
 * @link              http://rehan.work
 * @since             1.0.0
 * @package           Thrasio
 *
 * @wordpress-plugin
 * Plugin Name:       Thrasio
 * Plugin URI:        http://rehan.work/thrasio-plugin
 * Description:       This is a genrelised plugin that covers all the features sepecified by Thrasio Affiliate Marketing team.
 * Version:           1.0.0
 * Author:            Rehan Khan
 * Author URI:        http://rehan.work
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       thrasio
 * Domain Path:       /language
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'THRASIO_VERSION', '1.0.0' );
define( 'THRASIOPATH', plugin_dir_path(__FILE__) );
define( 'THRASIODOMAIN', 'thrasio' );

require_once( THRASIOPATH . '/deals/register.php' );
add_action( 'init', 'thrasio_register_deals_type' );
